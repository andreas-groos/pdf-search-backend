const fs = require("fs");
const path = require("path");
import readFiles from "./extractMeta";
import * as esConnection from "./connection";

/** Clear ES index, parse and index all files from the document directory */
async function readAndInsertDocuments() {
  try {
    // Clear previous ES index
    await esConnection.resetIndex();

    // Read textfile directory
    let documents = await readFiles();
    // Read each document file, and index each page in elasticsearch
    for (let file of documents) {
      console.log(`Reading File - ${file.filePath}`);
      const { title, author, summary, body, filePath } = file;
      const pages = parseDocumentBody(body);
      await insertDocumentData(title, author, summary, pages, filePath);
    }
    process.exit(0);
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
}

function parseDocumentBody(body) {
  const pages = body
    .replace(/Break-{4,}/g, "\n")
    .split(/-{4,}Page/)
    .map(line => line.replace(/\r\n/g, " ").trim()) // Remove paragraph line breaks and whitespace
    .map(line => line.replace(/_/g, ""))
    .filter(line => line && line !== ""); // Remove empty lines
  return pages;
}

/** Bulk index the book data in Elasticsearch */
async function insertDocumentData(title, author, summary, pages, filePath) {
  let bulkOps = []; // Array to store bulk operations

  // Add an index operation for each section in the book
  for (let i = 0; i < pages.length; i++) {
    // Describe action
    bulkOps.push({
      index: { _index: esConnection.index, _type: esConnection.type }
    });
    let filename = path.basename(filePath);
    filename = filename.replace(".content.txt", ``);
    // Add document
    bulkOps.push({
      author,
      title,
      summary,
      location: i + 1,
      filePath,
      filename,
      text: pages[i]
    });

    if (i > 0 && i % 500 === 0) {
      // Do bulk insert in 500 paragraph batches
      await esConnection.client.bulk({ body: bulkOps });
      bulkOps = [];
      console.log(`Indexed Pages ${i - 499} - ${i}`);
    }
  }

  // Insert remainder of bulk ops array
  await esConnection.client.bulk({ body: bulkOps });
  console.log(
    `Indexed Pages ${pages.length - bulkOps.length / 2} - ${pages.length}\n\n\n`
  );
}

readAndInsertDocuments();
