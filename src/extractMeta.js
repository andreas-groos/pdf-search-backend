const fs = require("fs");
const path = require("path");
const fm = require("front-matter");

const readFiles = async () => {
  const documents = [];
  try {
    // Read textfile directory
    let files = fs
      .readdirSync("./public/txt")
      .filter(file => file.slice(-4) === ".txt");
    console.log(`Found ${files.length} Files`);

    // Read each document file, and index each page in elasticsearch
    for (let file of files) {
      console.log(`Reading File - ${file}`);
      const filePath = path.join("./public/txt", file);
      const document = fs.readFileSync(filePath, "utf8");
      const frontMatter = await fm(document);
      let { title, author, summary, url } = frontMatter.attributes;
      if (!title) {
        title = "No title specified";
      }
      if (!author) {
        title = "No author specified";
      }
      if (!summary) {
        title = "No summary specified";
      }
      if (!url) {
        url = "No download URL specified";
      }
      const { body } = frontMatter;
      let filename = path.basename(filePath);
      filename = filename.replace(".content.txt", `.pdf`);
      const pages = body
        .replace(/Break-{4,}/g, "\n")
        .split(/-{4,}Page/)
        .map(line => line.replace(/\r\n/g, " ").trim()) // Remove paragraph line breaks and whitespace
        .map(line => line.replace(/_/g, ""))
        .filter(line => line && line !== "");
      documents.push({
        title,
        author,
        summary,
        body,
        filename,
        filePath,
        pages: pages.length,
        url
      });
    }
    return documents;
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
};

export default readFiles;
