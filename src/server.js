const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
require("dotenv").config();
const port = process.env.PORT ? process.env.PORT : 5000;
import { search, getParagraph } from "./search.js";
import readFiles from "./extractMeta";

let documents = [];
const fetchDocumentInfo = async () => {
  const tempDocs = await readFiles();
  tempDocs.forEach(t => {
    let { title, summary, author, pages, filename, url } = t;
    documents.push({ title, summary, author, pages, filename, url });
  });
  console.log("document info fetched");
};

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/split_pdf", express.static("public/split_pdfs"));
app.use("/pdf", express.static("public/pdf"));
app.use("/txt", express.static("public/txt"));

// LOGGING
app.use((req, res, next) => {
  console.log("req.method", req.method);
  next();
});

app.get("/hello", (req, res) => {
  res.json({ msg: "Hi there" });
});

app.get("/search", async (req, res) => {
  const { term, type } = req.query;
  const result = await search(term, type);
  console.log("result", result);
  res.json({ result });
});

app.get("/paragraphs", async (req, res) => {
  const { documentTitle, start, end } = req.query;
  console.log("term", term);
  console.log("offset", offset);
  const result = await getParagraph(term, offset);
  res.json({ result });
});

app.get("/documents", async (req, res) => {
  res.json({ documents });
});

fetchDocumentInfo();
app.listen(port, () => console.log(`Listening on port ${port}`));
