const { client, index, type } = require("./connection");

const search = (term, searchType) => {
  let body = {};
  if (searchType === "allwords" || searchType === "anywords") {
    body = {
      size: 5000,
      query: {
        match: {
          text: {
            query: term,
            operator: searchType === "allwords" ? "and" : "or",
            fuzziness: 0
            // fuzziness: "auto"
          }
        }
      },
      highlight: { fields: { text: {} } }
    };
  } else if (searchType === "term") {
    body = {
      size: 5000,
      query: {
        match_phrase: {
          text: term
        }
      },
      highlight: { fields: { text: {} } }
    };
  }
  let searchResult = client.search({ index, type, body });
  return searchResult;
};

const getParagraph = (documentTitle, startLocation, endLocation) => {
  const filter = [
    { term: { title: documentTitle } },
    { range: { location: { gte: startLocation, lte: endLocation } } }
  ];

  const body = {
    size: endLocation - startLocation,
    sort: { location: "asc" },
    query: { bool: { filter } }
  };
  const paragraphResults = client.search({ index, type, body });
  return paragraphResults;
};

export { search, getParagraph };
