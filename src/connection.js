const elasticsearch = require("elasticsearch");
require("dotenv").config();
// Core ES variables for this project
const index = "library";
const type = "document";
const port = 9200;
const host =
  process.env.NODE_ENV === "production" ? process.env.ELASTIC_URL : "localhost";
console.log("host", host);
const client = new elasticsearch.Client({ host: { host, port } });

/** Check the ES connection status */
async function checkConnection() {
  let isConnected = false;
  while (!isConnected) {
    console.log("Connecting to ES");
    try {
      const health = await client.cluster.health({});
      console.log(health);
      isConnected = true;
    } catch (err) {
      console.log("Connection Failed, Retrying...", err);
    }
  }
}

/** Clear the index, recreate it, and add mappings */
async function resetIndex() {
  if (await client.indices.exists({ index })) {
    await client.indices.delete({ index });
  }

  await client.indices.create({ index });
  await putDocumentMapping();
}

/** Add document section schema mapping to ES */
async function putDocumentMapping() {
  const schema = {
    title: { type: "keyword" },
    author: { type: "keyword" },
    summary: { type: "text" },
    location: { type: "integer" },
    filename: { type: "text" },
    text: { type: "text" }
  };

  return client.indices.putMapping({
    index,
    type,
    body: { properties: schema }
  });
}

export { client, index, type, checkConnection, resetIndex };
