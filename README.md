# PDF-SEARCH instructions

## Copy files to a server:

`scp *.pdf playground@159.89.132.153:pdf-search/pdf-search-backend/public/pdf`

## Add Swap

If you don't have a lot of memory you will want to add `swap`:

[https://linuxize.com/post/how-to-add-swap-space-on-ubuntu-18-04/](https://linuxize.com/post/how-to-add-swap-space-on-ubuntu-18-04/)

## Preparing the PDFs

### Necessary tools for preparing the PDFs

`npm install -g pdf2json` to install `pdf2json`

`sudo add-apt-repository ppa:malteworld/ppa`

`sudo apt-get install pdftk`

### Splitting PDFs up and extracting text

Making `processPDF.sh` executable: `chmod +x processPDF.sh`

Run `./processPDF.sh` from project directory with your PDFs already in 'public/pdf`.

You will be asked to supply name, author and summary for each PDF. It will extract the text content from the PDFs and split the PDFs up in single page PDF files.

The PDFs have to be processed with OCR beforehand.

## Getting `elasticsearch` up & running

`docker-compose up -d` from `/`

## Starting `express` server

Locally `npm run load && npm run dev`
