#!/bin/bash

cd public
mkdir txt
rm ./txt/*.txt
mkdir split_pdfs
rm ./split_pdfs/*.pdf
cd pdf
for f in *.pdf
do
        echo "Processing $f"
        name=$(echo "$f" | cut -f 1 -d '.')
        pdf2json -f $name.pdf -c   > /dev/null 2>&1
        mv $name.content.txt ../txt
        rm $name.json
        pdftk $f burst output ../split_pdfs/${name}_%04d.pdf   > /dev/null 2>&1
        cd ../txt
        echo Title of document:
        read title
        echo Author of document:
        read author
        echo Summary of document:
        read summary
        echo $'\n' | cat - $name.content.txt > temp && mv temp $name.content.txt
        echo $'---' | cat - $name.content.txt > temp && mv temp $name.content.txt
        echo summary: $summary | cat - $name.content.txt > temp && mv temp $name.content.txt
        echo author: $author | cat - $name.content.txt > temp && mv temp $name.content.txt
        echo title: $title | cat - $name.content.txt > temp && mv temp $name.content.txt
        echo $'---' | cat - $name.content.txt > temp && mv temp $name.content.txt
        cd ../pdf
done
